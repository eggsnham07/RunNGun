if [[ "$1" == "" ]]; then 
    echo "This command requires an argument!"
    exit 1
fi
git add .
git commit -m "$1"
git push gitlab testing
git push github testing