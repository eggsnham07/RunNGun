extends KinematicBody2D

export var speed:int = 200
export var jumpForce:int = 600
export var playerGravity:int = 800
export var killable:bool = true
export var movementAllowed:bool = true

var skin = "knight"
var velocity:Vector2 = Vector2()
var playerLevel:int = 0
var modifier:int = 2
var RunAnim:String = "KnightRun"
var IdleAnim:String = "KnightIdle"

onready var Body = $Body
onready var Weapon = $Weapon
onready var WSprite = Weapon.get_node("Sprite")
onready var Projectile = preload("res://Scenes/Player/Projectile.tscn")

func _ready():
	Weapon.position.x = 0
	Global.Player = self
	print(Global.get_skin())
	skin = Global.get_skin()
	
	if skin == "nerd": Weapon.set_sprite(load("res://Assets/Images/Keyboard.png"))
	if skin == "knight": Weapon.set_sprite(load("res://Assets/Images/Sword.png"))
	if skin == "monk": Weapon.set_sprite(load("res://Assets/Images/Stick.png"))
	
	RunAnim = skin.capitalize() + "Run"
	IdleAnim = skin.capitalize() + "Idle"
	

func _physics_process(delta):
	
	var proj = Projectile.instance()
	if Body.flip_h: modifier = -50
	else: modifier = 50
	velocity.y += playerGravity * delta
	velocity.x = 0
	
	if movementAllowed and Input.is_action_pressed("right"):
		velocity.x += speed
	if movementAllowed and Input.is_action_pressed("left"):
		velocity.x -= speed
	if movementAllowed and Input.is_action_pressed("jump") and is_on_floor():
		velocity.y -= jumpForce
		
	if movementAllowed and Input.is_action_pressed("down"):
		playerGravity = 2800
	elif movementAllowed and not Input.is_action_pressed("down"):
		playerGravity = 800
		
	#if movementAllowed and Input.is_action_just_pressed("shoot"):
	#	var sfx = preload("res://Assets/SFX/Pew.tscn")
	#	get_parent().add_child(proj)
	#	get_parent().add_child(sfx.instance())
	#	proj.global_position.x = get_parent().get_node("Player/Body/Weapon").global_position.x + modifier
	#	proj.global_position.y = get_parent().get_node("Player/Body/Weapon").global_position.y
	#	if Body.flip_h == false: proj.reverse = true
	#	else: proj.reverse = false
	
	if movementAllowed and Input.is_action_pressed("shoot"):
		Weapon.position.y = -10
		if WSprite.flip_h:
			Weapon.set_rotation_degrees(-45)
			Weapon.position.x = -40
		else: 
			Weapon.set_rotation_degrees(45)
			Weapon.position.x = 40
			
	if movementAllowed and not Input.is_action_pressed("shoot"):
		Weapon.set_rotation_degrees(0)
		Weapon.position.x = 0
		
	if movementAllowed and Input.is_action_pressed("sprint"):
		speed = 300
	if movementAllowed and not Input.is_action_pressed("sprint"):
		speed = 200
		
	if WSprite.flip_h and not Input.is_action_pressed("shoot"):
		WSprite.position.x = -25
	else:
		if not Input.is_action_pressed("shoot"):
			WSprite.position.x = 25
	
	if velocity.x > 0:
		Body.flip_h = false
		WSprite.flip_h = false
		if not Input.is_action_pressed("shoot"): Weapon.position.y = 2.8
	elif velocity.x < 0:
		Body.flip_h = true
		WSprite.flip_h = true
		if not Input.is_action_pressed("shoot"): Weapon.position.y = 2.8
	
	if velocity.x == 0:
		Body.play(IdleAnim)
		if not Input.is_action_pressed("shoot"): Weapon.position.y = 6.8
	else:
		Body.play(RunAnim)
		
	velocity = move_and_slide(velocity, Vector2.UP)
	
func die():
	if killable:
		movementAllowed = false
		position = Vector2(-900, -900)
		get_parent().get_node("PlayerCam/YouDied").show()
		get_parent().get_node("PlayerCam/YouDied").hidden = false
