extends Control

func _ready():
	Global.Player = null

func _on_Knight_pressed():
	Global.write_file("user://settings.json", String(Global.read_file("user://settings.json")).replace("--skin=" + Global.get_skin(), "--skin=knight"))
	Global.reload_settings(false)
	_on_Back_pressed()

func _on_Nerd_pressed():
	Global.write_file("user://settings.json", String(Global.read_file("user://settings.json")).replace("--skin=" + Global.get_skin(), "--skin=nerd"))
	Global.reload_settings(false)
	_on_Back_pressed()

func _on_Monk_pressed():
	Global.write_file("user://settings.json", String(Global.read_file("user://settings.json")).replace("--skin=" + Global.get_skin(), "--skin=monk"))
	Global.reload_settings(false)
	_on_Back_pressed()

func _on_Back_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")
