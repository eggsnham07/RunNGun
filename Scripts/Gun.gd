extends Node2D

func _on_Area2D_area_entered(area):
	if area.enemy == true and Input.is_action_pressed("shoot"):
		area.die()
		
func set_sprite(sprite:Texture):
	$Sprite.texture = sprite

func get_sprite():
	return $Sprite.texture.resource_name.to_lower()
