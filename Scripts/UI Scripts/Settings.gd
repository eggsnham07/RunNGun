extends Control
"""
	Settings page script
"""
onready var vsync = get_node("VSyncLabel/VSync")
onready var fullscreen = get_node("FSLabel/FullScreen")

func _ready():
	if Global.fileDoesExist("user://framerate.cfg"): get_node("Framerate/FramerateInput").text = String(Global.read_file("user://framerate.cfg"))
	vsync.pressed = OS.vsync_enabled
	fullscreen.pressed = OS.window_fullscreen
	
func _process(_delta):
	var event = Input
	if event.is_action_just_pressed("controller_back"):
		get_tree().change_scene("res://Scenes/Menu.tscn")

func _on_VSync_toggled(button_pressed):
	if button_pressed:
		Global.write_file("user://settings.json", JSON.print(Global.get_settings().append("--vsync").join(",\n\t")))
	else:
		Global.write_file("user://settings.json", JSON.print(Global.get_settings().join(",\n\t").replace("--vsync,\n\t", "")))
		
	Global.reload_settings(false)

func _on_FullScreen_toggled(button_pressed):
	Global.write_file("user://fullscreen.cfg", String(button_pressed))
	if Global.debug_mode:
		print("Fullscreen Toggled: " + String(button_pressed))
		
	Global.reload_settings(false)

func _on_Update_pressed():
	Global.write_file("user://framerate.cfg", get_node("Framerate/FramerateInput").text)
	if Global.debug_mode:
		print(float(get_node("Framerate/FramerateInput").text))
		
	Global.reload_settings(false)


func _on_Button_pressed():
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_Reset_pressed():
	Global.clear_settings()
